//
//  TweetsDetailViewController.swift
//  betvictor
//
//  Created by Nicholas Caruso on 01/01/2020.
//  Copyright © 2020 Nicholas Caruso. All rights reserved.
//

import UIKit
import MapKit

class TweetsDetailViewController: BaseViewController, UITableViewDataSource,UITableViewDelegate {

    //I am using a Table view with one custom cell, it is helpful cause TableView provides native Scroll
    @IBOutlet weak var tableView: UITableView!
    
    private var viewModel: TweetsDetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initControls()
    }
    
    //MARK: - Private methods
    private func initControls(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 500

    }
    
    //MARK: - Public methods
    public func setTweetStatus(tweetStatus: Status){
        viewModel = TweetsDetailViewModel(tweetStatus: tweetStatus)
    }
    
    //MARK: - UI Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel?.getNumberOfSections() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch viewModel?.getTableSections()[indexPath.row]{
            //First Section of View Controller
        case TweetsDetailViewModel.detailInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailInfoCell",for: indexPath) as! TweetDetailInfoCell
            cell.userAvatar.setAvatarImage(viewModel?.tweetStatus.user.profile_image_url)
            cell.userName.text = ("User: \(viewModel?.tweetStatus.user.name ?? "")")
            cell.location.text = ("Location: \(viewModel?.tweetStatus.user.location ?? "")")
            cell.followersCount.text = ("Followers Count: \(viewModel?.tweetStatus.user.followers_count ?? 0)")
            cell.tweetCount.text = ("Tweet Count: \(viewModel?.tweetStatus.user.statuses_count ?? 0)")
            cell.mapView.isHidden = true
            cell.favouriteCount.isHidden = true
            cell.retweetCount.isHidden = true
            cell.showMoreTapped = {[weak self] in
                cell.showMoreBtn.isHidden = true
                if (self?.viewModel?.tweetStatus.geo) != nil{
                    cell.mapView.isHidden = false
                    //add Pin
                    let annotation = MKPointAnnotation()
                    if let coordinates = self?.viewModel?.tweetStatus.coordinates {
                        annotation.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(coordinates[0]), longitude: CLLocationDegrees(coordinates[1]))
                        cell.mapView.addAnnotation(annotation)
                    }

                 }
                else{
                    //Map is Hidden
                    cell.mapView.isHidden = true
                    cell.favouriteCountLabelTopConst.constant = 80
                }
                cell.favouriteCount.text = ("Favourite Count: \(self?.viewModel?.tweetStatus.user.favourites_count ?? 0)")
                cell.retweetCount.text = ("Retweet Count: \(self?.viewModel?.tweetStatus.retweet_count ?? 0)")
                cell.favouriteCount.isHidden = false
                cell.retweetCount.isHidden = false
            }
            return cell
        default :
            return UITableViewCell()
            }
        }
    
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        switch viewModel?.getTableSections()[indexPath.row]{
//        case TweetsDetailViewModel.detailInfo:
//            return 180
//        default:
//            let bounds = UIScreen.main.bounds
//            let height = bounds.size.height
//            return height - 140
//        }
//    }
}
