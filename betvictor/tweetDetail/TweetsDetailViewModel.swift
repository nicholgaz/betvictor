//
//  TweetsDetailViewModel.swift
//  betvictor
//
//  Created by Nicholas Caruso on 01/01/2020.
//  Copyright © 2020 Nicholas Caruso. All rights reserved.
//

import Foundation

class TweetsDetailViewModel {
    
    private var sections : [TweetDetailSection] = [.detailInfo]
    
    public static let detailInfo = TweetDetailSection.detailInfo    
    enum TweetDetailSection: String {
        case detailInfo = "DetailInfo"
    
    }
    
    public var tweetStatus: Status
    
    init(tweetStatus: Status)
    {
        self.tweetStatus = tweetStatus
    }
    
    public func getNumberOfSections() -> Int
    {
        return self.sections.count
    }
    
    public func getTableSections() -> [TweetDetailSection]
    {
        return self.sections
    }
}
