//
//  TweetDetailInfoCell.swift
//  betvictor
//
//  Created by Nicholas Caruso on 01/01/2020.
//  Copyright © 2020 Nicholas Caruso. All rights reserved.
//

import UIKit
import MapKit

class TweetDetailInfoCell: UITableViewCell {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var followersCount: UILabel!
    @IBOutlet weak var tweetCount: UILabel!
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var showMoreBtn: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var favouriteCount: UILabel!
    @IBOutlet weak var retweetCount: UILabel!
    @IBOutlet var favouriteCountLabelTopConst: NSLayoutConstraint!
    
    var showMoreTapped: (() -> Void)?

    @IBAction func showMoreClicked(sender: AnyObject) {
       showMoreTapped?()
    }
        
}
