//
//  Root.swift
//  betvictor
//
//  Created by Nicholas Caruso on 28/12/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation

struct TweetSearch: Decodable {
    let statuses: [Status]?
}
