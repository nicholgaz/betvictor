//
//  Status.swift
//  betvictor
//
//  Created by Nicholas Caruso on 27/12/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation

struct Status: Decodable, Hashable {

    let id : Int
    let user: User
    let text: String
    let truncated : Bool
    let geo: Bool?
    let coordinates: [Int]?
    let retweet_count: Int
    
    static func == (lhs: Status, rhs: Status) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(user)
        hasher.combine(text)
        hasher.combine(truncated)
        hasher.combine(geo)
        hasher.combine(coordinates)
        hasher.combine(retweet_count)
    }
}
