//
//  User.swift
//  betvictor
//
//  Created by Nicholas Caruso on 29/12/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation

struct User: Decodable, Hashable {
    let id: Int
    let name: String
    let location: String
    let profile_image_url: String
    let followers_count: Int
    let statuses_count: Int
    let favourites_count: Int
    
    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(name)
        hasher.combine(location)
        hasher.combine(profile_image_url)
        hasher.combine(followers_count)
        hasher.combine(statuses_count)
        hasher.combine(favourites_count)
    }
}
