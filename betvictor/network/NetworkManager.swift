//
//  NetworkManager.swift
//  betvictor
//
//  Created by Nicholas Caruso on 24/12/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation
import OhhAuth

class NetworkManager {

    public static let sharedIstance = NetworkManager()
    
    private static let kApiServer = "https://api.twitter.com/1.1/search/tweets.json"


    private init() {
    }
    
    private func createSearchParameters() {
        
    }
    
    private static func getSearchParameters(parameters: [String: String]) ->String {
        var param: String = "?"
        let searchKey = Array(parameters.keys)[0]
        param += searchKey
        param += "="
        param += parameters[searchKey] ?? "q"
        return param
    }
    
    public static func performTweetRequest(parameters: [String: String], _ onSuccess: @escaping(Any) -> Void, onFailure: @escaping(Error) -> Void) {
    
        if !NetworkUtil.connectedToNetwork()
        {
            onFailure(NetworkError.connectionError("No Internet Connection"))
            return
        }
        let url = URL(string: kApiServer+getSearchParameters(parameters: parameters))!
        let cc = (key: "gYX72aW5cnyThydgQvqb524V6", secret: "qc9Y5iFlJFnBMBC4fE44AOPf7SyU9AbYLDWLfPwRiNZwGvnY2l")
        let uc = (key: "382750449-XnJUlf2ByAvXrOxWplw9aaZDOKhZVwJGYCCVRwcv", secret: "hXpp423AN0y2PjUL2zy90JdMZcS8LlTzGQPoLgetUraQH")
        var request = URLRequest(url: url)
        request.oAuthSign(method: "GET", consumerCredentials: cc, userCredentials: uc)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil else {                                              // check for fundamental networking error
                print("error", error ?? "Unknown error")
                    onFailure(NetworkError.HTTPError("An error occurred while processing request"))
                return
            }

            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                onFailure(NetworkError.HTTPError("An error occurred while processing request"))
                return
            }

            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(responseString)")
            
            do {
                let decoder = JSONDecoder.init()
                let result: TweetSearch? = try decoder.decode(TweetSearch.self, from: data)
                print("XXX Tweets \(result)")
                onSuccess(result!)
            } catch let e {
                print(e)
            }
        }
        task.resume()
    }
    

}
extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="

        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
