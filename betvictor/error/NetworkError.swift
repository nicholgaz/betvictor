//
//  APIError.swift
//  betvictor
//
//  Created by Nicholas Caruso on 30/12/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation
enum NetworkError: Error {
    case connectionError(String)
    case HTTPError(String)
}
