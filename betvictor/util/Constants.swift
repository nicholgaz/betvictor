//
//  Constants.swift
//  betvictor
//
//  Created by Nicholas Caruso on 28/12/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation

struct K {
    struct Endpoint {
        static let baseURL = "https://api.twitter.com/1.1"
    }
    
    struct APIParameters {

    }
    
    struct TextValues {

    }
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}
