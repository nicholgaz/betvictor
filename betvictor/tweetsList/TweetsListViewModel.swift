//
//  TweetsListViewModel.swift
//  betvictor
//
//  Created by Nicholas Caruso on 29/12/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation

class TweetsListViewModel {

    public var tweetsStatus: [Status]?
    public var previousTweetsStatus: [Status] = [Status]()
    public var searchedText = [String]()

        //MARK: - constructor

    init()
    {
    }

    // When a new search is triggered I cut the twitts that already were shown in the previous search
    private func filterTweetsArray()-> [Status]{
        if let tweetsStatus = self.tweetsStatus {
            var currentSearch = Set<Status>(tweetsStatus)
            let previousSearch = Set(previousTweetsStatus)

            // Return a set with all values in A which are not contained in B
            currentSearch.subtract(previousSearch)
            let filteredSearch = Array(currentSearch)
            return filteredSearch
        }
        return [Status]()
    }
    
    //MARK: - Private Methods

    
    private func createSearchParameters(searchValues: SearchTweetsValues) -> [String:String] {
        return ["q": searchValues.rawValue]
    }
    
    //MARK: - Public Methods
        
    public func performTweetSearchRequest(values: SearchTweetsValues,previousSearch: Bool,  completionHandler:@escaping((_:Bool, _:Error?) -> Void))
    {
        NetworkManager.performTweetRequest(parameters: createSearchParameters(searchValues: values), { (result) in
            DispatchQueue.main.async{
                if let tweetsStatus = (result as! TweetSearch).statuses{
                        self.tweetsStatus = tweetsStatus
                        if(previousSearch){
                            self.tweetsStatus = self.filterTweetsArray()
                        }
                        self.previousTweetsStatus = tweetsStatus
                    }
                    else{
                        self.tweetsStatus = [Status]()
                        self.previousTweetsStatus = [Status]()
                    }

                    completionHandler(true, nil)
            }
        }) { (error) in
            DispatchQueue.main.async {
                completionHandler(false, error)
              }
        }
    }

    
}
