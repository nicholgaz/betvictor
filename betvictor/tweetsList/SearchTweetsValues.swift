//
//  SearchTweetsValues.swift
//  betvictor
//
//  Created by Nicholas Caruso on 31/12/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation

enum SearchTweetsValues: String {
    case me = "me"
    case i = "I"
    case meAndI = "me,I"
}
