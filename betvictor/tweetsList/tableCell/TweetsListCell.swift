//
//  TweetsListCell.swift
//  betvictor
//
//  Created by Nicholas Caruso on 30/12/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import UIKit

class TweetsListCell: UITableViewCell {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var tweetText: UITextView!
    @IBOutlet weak var userAvatar: UIImageView!
}
