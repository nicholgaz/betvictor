//
//  TweetsListViewController.swift
//  betvictor
//
//  Created by Nicholas Caruso on 29/12/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import UIKit

class TweetsListViewController: BaseViewController, UITableViewDataSource,UITableViewDelegate {

    private weak var timer: Timer?

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchIBtn: UIButton!
    @IBOutlet weak var searchMeBtn: UIButton!
    @IBOutlet weak var searchIAndMeBtn: UIButton!
    @IBOutlet weak var viewTitle: UILabel!
    
    
    private var viewModel: TweetsListViewModel = TweetsListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initControls()
    }
    
    
    deinit {
        stopTimer()
    }
    //MARK: - Private methods
    private func initControls(){
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        searchIBtn.tag = 0
        searchIBtn.addTarget(self, action: #selector(searchButtonClicked(_:)), for: .touchUpInside)
        searchMeBtn.tag = 1
        searchMeBtn.addTarget(self, action: #selector(searchButtonClicked(_:)), for: .touchUpInside)
        searchIAndMeBtn.tag = 2
        searchIAndMeBtn.addTarget(self, action: #selector(searchButtonClicked(_:)), for: .touchUpInside)
        
        viewTitle.text = "Search your tweets"

    }
    
    private func fetchTweetsList(searchValues: SearchTweetsValues, previousSearch: Bool) {
        let id = UIViewController.postShowSpinnerNotification(message: "loading")
        self.viewModel.performTweetSearchRequest(values: searchValues,previousSearch: previousSearch, completionHandler: { (status, error) in
            UIViewController.postHideSpinnerNotification(spinnerId: id)
            if status
            {
                self.tableView.reloadData()

                if previousSearch == false || self.timer == nil{
                    if self.timer != nil {
                        self.stopTimer()
                    }
                    self.startTimer(searchValues: searchValues)
                }
            }
        })
    }
    
    private func startTimer(searchValues: SearchTweetsValues) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true) { [weak self] _ in
            self?.fetchTweetsList(searchValues: searchValues, previousSearch: true)
        }
    }

    private func stopTimer() {
        timer?.invalidate()
    }
    //MARK: - UIButton callback
    @objc private func searchButtonClicked(_ sender : UIButton) {
        switch sender.tag {
            case 0 :
                viewModel.searchedText.removeAll()
                viewModel.searchedText.append(SearchTweetsValues.i.rawValue)
                self.fetchTweetsList(searchValues: .i,previousSearch: false)
                return
            case 1 :
                viewModel.searchedText.removeAll()
            viewModel.searchedText.append(SearchTweetsValues.me.rawValue)
                self.fetchTweetsList(searchValues: .me,previousSearch: false)
                return
            default :
                viewModel.searchedText.removeAll()
            viewModel.searchedText.append(SearchTweetsValues.i.rawValue)
            viewModel.searchedText.append(SearchTweetsValues.me.rawValue)
                self.fetchTweetsList(searchValues: .meAndI,previousSearch: false)
        }
    }
    
    //MARK: - UI Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tweetsStatus?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let tweetListCell=tableView.dequeueReusableCell(withIdentifier: "tweetsListCell",for: indexPath) as! TweetsListCell
        guard let tweetsStatus = self.viewModel.tweetsStatus else {
            self.viewModel.tweetsStatus = [Status]()
            return tweetListCell
        }
        let tweet = tweetsStatus[indexPath.row] as Status
        tweetListCell.userName.text = tweet.user.name
        tweetListCell.tweetText.makeBold(originalText: tweet.text, boldCharacters: viewModel.searchedText)
        

        tweetListCell.userAvatar.setAvatarImage(tweet.user.profile_image_url)
        
       return tweetListCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }

    //MARK: - UITable View Data Source
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        guard let selectedTweet = viewModel.tweetsStatus?[indexPath.row] else { return}
        let viewController = TweetsDetailViewController.loadFromStoryboard()
        viewController.setTweetStatus(tweetStatus: selectedTweet)
        self.navigationController?.pushViewController(viewController, animated: true)
    }

}
