//
//  BaseViewController.swift
//  betvictor
//
//  Created by Nicholas Caruso on 30/12/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import UIKit
class BaseViewController: UIViewController, StoryboardLoad
{
    internal var isNavBarHidden = false
    //MARK: - View lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.initNavBarVisibility()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
    }
    
    //MARK - Private methods
    
    
    private func initNavBarVisibility()
    {
        guard let navCont = self.navigationController else {
            return
        }
        navCont.isNavigationBarHidden = self.isNavBarHidden
    }
    
    //MARK - Internal methods
    
    internal func hideNavBarIfPossible(animated:Bool)
    {
        if !self.isNavBarHidden
        {
            if let navCont = self.navigationController, !navCont.isNavigationBarHidden
            {
                navCont.setNavigationBarHidden(true, animated: animated)
            }
        }
    }
    
    internal func showNavBarIfPossible(animated:Bool)
    {
        if let navCont = self.navigationController, navCont.isNavigationBarHidden
        {
            navCont.setNavigationBarHidden(self.isNavBarHidden, animated: animated)
        }
    }
    
}
