//
//  UIViewController.swift
//  betvictor
//
//  Created by Nicholas Caruso on 30/12/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//


import UIKit
public extension UIViewController {
    static func postShowSpinnerNotification(message: String?) -> String {
        let id: String = UUID().uuidString as String
        var userInfo:[String:Any] = ["id": id]
        if let message  = message {
            userInfo["message"] = message
    }
    
    let notification = Notification(name: Notification.Name("showSpinner"), object: nil, userInfo: userInfo)
    NotificationCenter.default.post(notification)
    return id
    }

    static func postHideSpinnerNotification(spinnerId: String?){
        var userInfo:[String:Any]?
        if let id = spinnerId
        {
            userInfo = ["id": id]
        }
        let notification = Notification(name: Notification.Name("hideSpinner"), object: nil, userInfo: userInfo)
        NotificationCenter.default.post(notification)
    }

}
