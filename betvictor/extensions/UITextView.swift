//
//  UITextView.swift
//  betvictor
//
//  Created by Nicholas Caruso on 04/01/2020.
//  Copyright © 2020 Nicholas Caruso. All rights reserved.
//

import UIKit

extension UITextView {
    func makeBold(originalText: String, boldCharacters: [String]) {
        let attributedOriginalText = NSMutableAttributedString(string: originalText)
        for char in boldCharacters {
            for match in createRegex(boldText: char).matches(in: originalText, range: NSRange(0..<originalText.utf16.count)) {
                attributedOriginalText.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 14), range: match.range)
            }
        }
        self.attributedText = attributedOriginalText
    }
    
    private func createRegex(boldText: String)-> NSRegularExpression {
        let searchPattern = "\\b"+NSRegularExpression.escapedPattern(for: boldText)+"\\b"
        let regex = try! NSRegularExpression(pattern: searchPattern, options: .caseInsensitive)
        return regex
    }
}
