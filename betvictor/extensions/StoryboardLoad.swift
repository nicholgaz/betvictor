//
//  StoryboardLoad.swift
//  betvictor
//
//  Created by Nicholas Caruso on 30/12/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import UIKit
import Foundation

protocol StoryboardLoad: class
{
    /** The storyboard idendifier */
    static var storyboardId: String {get}
    
    /** The controller identifier in the storyboard */
    static var storyboardControllerId: String {get}
}

extension StoryboardLoad where Self: UIViewController
{
    static var storyboardId: String
    {
        return "Main"
    }
    
    static var storyboardControllerId: String
    {
        return String(describing: self)
    }
    
    static func loadFromStoryboard() -> Self
    {
        let bundle = Bundle.main
        
        let storyboard = UIStoryboard(name: Self.storyboardId, bundle: bundle)
        
        let vc = storyboard.instantiateViewController(withIdentifier: Self.storyboardControllerId) as! Self
        
        return vc
    }
}
